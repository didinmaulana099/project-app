<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use DB;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

    	for($i = 1; $i <= 50; $i++){

    	      // insert data ke table pegawai menggunakan Faker
    		DB::table('article')->insert([
    			'title' => $faker->text(50),
    			'content' => $faker->text,
    			'creator' => $faker->name,
    			'image' => "",
                'path' => "https://via.placeholder.com/600/771796"
    		]);

    	}
    }
}
