<?php

namespace App\Http\Controllers;

use App\Events\ArticleEvent;
use Illuminate\Http\Request;
use App\Models\Article;
use App\Http\Requests\StoreArticle;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Cache;

class ArticleController extends Controller
{
    public function index(Request $request){
        $article = Article::orderBy('id', 'desc')->paginate(10);
        return view('pages.index', compact('article'));
    }

    public function store(StoreArticle $request){
        $article = new Article();
        $article->title = $request->title;
        $article->content = $request->content;
        $article->creator = $request->creator;

        $file = $request->file('image');
        $file_extension = $file->extension();
        $filename = rand(0, 99).time().'.'.$file_extension;
        $file->storeAs('public/images', $filename);
        //file path image upload
        $file_path = "storage/app/public/images/{$filename}";
        $fileServerpath = asset($file_path);
        $article->path = $fileServerpath;
        $article->image = $filename;

        $article->save();

        ArticleEvent::dispatch($article);

        return redirect('/article')->with('success', 'Article Uploaded successfully.');

    }

    public function edit($id){
        $article = Article::find($id);
        return view('pages.edit', compact('article'));
    }

    public function update(Request $request, $id){
        $article = Article::find($id);
        $article->title = $request->title;
        $article->content = $request->content;
        $article->creator = $request->creator;

        if (!empty($request->file('image'))) {
            $file = $request->file('image');
            $file_extension = $file->extension();
            $filename = rand(0, 99).time().'.'.$file_extension;
            $file->storeAs('public/images', $filename);
            //file path image upload
            $file_path = "storage/app/public/images/{$filename}";
            $fileServerpath = asset($file_path);
            $article->path = $fileServerpath;
            $article->image = $filename;
        }

        $article->save();

        return redirect('/article')->with('success', 'Article Updated successfully.');
    }

    public function destroy($id){

        $article = Article::find($id);

        if (!empty($article->image)) {
            Storage::delete('public/images/'. $article->image);
        }

        $article->delete();

        return redirect('/article')->with('success', 'Article Deleted successfully.');
    }

}
