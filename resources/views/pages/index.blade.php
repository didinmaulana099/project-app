@extends('master.layout')

@section('content')

<div class="content-wrapper">

    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            @if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
                @php
                Session::forget('success');
                @endphp
            </div>
            @endif
            <div class="card-body">
                <h4 class="card-title">Data Article</h4>
                <button type="button" class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#exampleModal">
                    Tambah
                </button>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th> Article Image </th>
                            <th> Article Creator </th>
                            <th> Title </th>
                            <th> Content </th>
                            <th> Action </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($article as $key => $item )

                        <tr>
                            <td>{{$key + 1}}</td>
                            <td class="py-1">
                                <img src="{{$item->path}}" alt="image" />
                            </td>
                            <td>{{$item->title}}</td>
                            <td>{{$item->creator}}</td>
                            <td>
                                <details>
                                    <summary>click</summary>
                                    {!! $item->content !!}
                                </details>
                            </td>
                            <td>
                                <form action="{{ route('article.destroy', $item->id) }}" method="post">
                                    @csrf
                                    @method('delete')
                                    <a href="{{ route('article.edit', $item->id) }}"
                                        class="btn btn-sm btn-info">
                                        <i class="mdi mdi-table-edit"></i>
                                    </a>
                                    <button type="submit" class="btn btn-sm btn-danger"
                                    onclick="return confirm('Apakah Anda Yakin?')">
                                    <i class="mdi mdi-table-row-remove"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <p>{!! $article->withQueryString()->links('pagination::bootstrap-5') !!}</p>
        </div>

    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-body">
                <div class="col-12 grid-margin stretch-card">

                    <div class="card">
                        <div class="modal-header">
                            <h4 class="card-title">Add Article</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="card-body">
                            <form method="POST" class="forms-sample row g-3" action="{{ route('article.store')}}" enctype='multipart/form-data'>
                                @csrf
                                <div class="form-group col-md-6">
                                    <label for="">Upload Image</label>
                                    <input type="file" name="image" id="inputImage" class="form-control @error('image') is-invalid @enderror">

                                    @error('image')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="">Creator</label>
                                    <input type="text" name="creator" id="inputCreator" class="form-control @error('creator') is-invalid @enderror" placeholder="creator sample">

                                    @error('creator')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="">Title</label>
                                    <input type="text" name="title" id="inputTitle" class="form-control @error('title') is-invalid @enderror" placeholder="title sample">

                                    @error('title')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="exampleTextarea1">Textarea</label>
                                    <textarea id="konten" class="form-control @error('content') is-invalid @enderror" name="content" rows="10" cols="50"></textarea>

                                    @error('content')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="form-group col-md-12">
                                    <button type="submit" class="btn btn-gradient-primary me-2">Submit</button>
                                    <button class="btn btn-light">Cancel</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
</div>

@endsection
