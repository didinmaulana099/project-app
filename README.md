<h3>STEP BY STEP INSTALASI</h3>
<p>versi php yang di gunakan <b>PHP 8.0.19</b></p>
<p>versi laravel yang di gunakan <b>Laravel Framework 9.43.0</b></p>
<p>--------------------------</p>
<p>composer update<p>
<p>setting <b>.env</b> untuk merubah nama database</p>
<p>jalankan pada terminal <b>php artisan migrate</b> untuk membuat table di database</p>
<p>jalankan pada terminal <b>php artisan db:seed</b> untuk membuat isi table</p>
<p>jalankan pada terminal <b>php artisan storage:link<b> untuk membuka penyimpanan images di storage</p>
<p> clear cache untuk mengantisipasi adanya kesalahan pada perubahan <b>php artisan config:cache<b></p>
<p>running project = php -S iplocal:8000 contoh <b> php -S 192.168.167.39:8000</b><p>
<p>-----------------------------</p>
<h3>untuk melihat tampilan depan, atau frontend clone dengan link git berikut:</h3>
<a href="https://gitlab.com/didinmaulana099/project-fe.git"></a>
<p>jalankan <b>npm install</b> pada terminal untuk install package manager</p>
<p>running project <b>yarn dev</b></p>
