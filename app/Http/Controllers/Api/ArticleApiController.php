<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Article;
use Illuminate\Support\Facades\Cache;

class ArticleApiController extends Controller
{
    public function index(Request $request){
        $data = Article::orderBy('id', 'desc')->paginate();
        return response()->json($data);
    }

}
