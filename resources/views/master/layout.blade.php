<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>GO Kampus</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{ asset('public/assets/vendors/mdi/css/materialdesignicons.min.css')}}">
    <link rel="stylesheet" href="{{ asset('public/assets/vendors/css/vendor.bundle.base.css')}}">

    <link rel="stylesheet" href="{{ asset('public/assets/css/style.css')}}">
    <link rel="shortcut icon" href="{{ asset('public/assets/images/favicon.ico')}}"/>

</head>

<body>
    <div class="container-scroller">

        <nav class="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
            <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
                <a class="navbar-brand brand-logo" href="index.html"><img src="{{ asset('public/assets/images/logo.svg')}}" alt="logo" /></a>
                <a class="navbar-brand brand-logo-mini" href="index.html"><img src="{{ asset('public/assets/images/logo-mini.svg')}}" alt="logo" /></a>
            </div>
            <div class="navbar-menu-wrapper d-flex align-items-stretch">
                <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
                    <span class="mdi mdi-menu"></span>
                </button>

                <ul class="navbar-nav navbar-nav-right">
                    <li class="nav-item nav-profile dropdown">
                        <a class="nav-link dropdown-toggle" id="profileDropdown" href="#" data-bs-toggle="dropdown" aria-expanded="false">
                            <div class="nav-profile-img">
                                <img src="{{ asset('public/assets/images/faces/face1.jpg')}}" alt="image">
                                <span class="availability-status online"></span>
                            </div>
                            <div class="nav-profile-text">
                                <p class="mb-1 text-black">{{ Auth::user()->name }}</p>
                            </div>
                        </a>
                        <div class="dropdown-menu navbar-dropdown" aria-labelledby="profileDropdown">
                            <a class="dropdown-item" href="#">

                                <a class="dropdown-item" href="{{route('logout')}}">
                                    <i class="mdi mdi-logout me-2 text-primary"></i> Signout </a>
                                </div>
                            </li>

                        </ul>
                        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
                            <span class="mdi mdi-menu"></span>
                        </button>
                    </div>
                </nav>

                <div class="container-fluid page-body-wrapper">

                    <nav class="sidebar sidebar-offcanvas" id="sidebar">
                        <ul class="nav">
                            <li class="nav-item nav-profile">
                                <a href="#" class="nav-link">
                                    <div class="nav-profile-image">
                                        <img src="{{ asset('public/assets/images/faces/face1.jpg')}}" alt="profile">
                                        <span class="login-status online"></span>

                                    </div>
                                    <div class="nav-profile-text d-flex flex-column">
                                        <span class="font-weight-bold mb-2">{{ Auth::user()->name }}</span>
                                        {{-- <span class="text-secondary text-small">Project Manager</span>  --}}
                                    </div>
                                    <i class="mdi mdi-bookmark-check text-success nav-profile-badge"></i>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.html">
                                    <span class="menu-title">Dashboard</span>
                                    <i class="mdi mdi-home menu-icon"></i>
                                </a>
                            </li>

                        </ul>
                    </nav>
                    <!-- partial -->
                    <div class="main-panel">

                        @yield('content')

                        <footer class="footer">
                            <div class="container-fluid d-flex justify-content-between">
                                <span class="text-muted d-block text-center text-sm-start d-sm-inline-block">Copyright © bootstrapdash.com 2021</span>
                                <span class="float-none float-sm-end mt-1 mt-sm-0 text-end"> Free <a href="https://www.bootstrapdash.com/bootstrap-admin-template/" target="_blank">Bootstrap admin template</a> from Bootstrapdash.com</span>
                            </div>
                        </footer>
                        <!-- partial -->
                    </div>
                </div>
            </div>


            <script src="{{ asset('public/assets/vendors/js/vendor.bundle.base.js')}}"></script>
            <script src="{{ asset('public/assets/vendors/chart.js/Chart.min.js')}}"></script>
            <script src="{{ asset('public/assets/js/jquery.cookie.js" type="text/javascript')}}"></script>
            <script src="{{ asset('public/assets/js/off-canvas.js')}}"></script>
            <script src="{{ asset('public/assets/js/hoverable-collapse.js')}}"></script>
            <script src="{{ asset('public/assets/js/misc.js')}}"></script>
            <script src="{{ asset('public/assets/js/dashboard.js')}}"></script>
            <script src="{{ asset('public/assets/js/todolist.js')}}"></script>

            <script src="{{asset('public/assets/ckeditor/ckeditor.js')}}"></script>
            <script>
            var konten = document.getElementById("konten");
                CKEDITOR.replace(konten,{
                language:'en-gb'
            });
            CKEDITOR.config.allowedContent = true;
            </script>
        </body>
        </html>
