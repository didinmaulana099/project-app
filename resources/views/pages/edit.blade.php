@extends('master.layout')

@section('content')
<div class="content-wrapper">

    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            @if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
                @php
                Session::forget('success');
                @endphp
            </div>
            @endif
            <div class="card-body">
                <h4 class="card-title">Edit Article</h4>
                <br/>
                <form method="POST" class="forms-sample row g-3" action="{{ route('article.update', $article->id)}}" enctype='multipart/form-data'>
                    @csrf
                    @method('put')
                    <div class="form-group col-md-6">
                        <label for="">Upload Image</label>
                        <input type="file" name="image" id="inputImage" class="form-control @error('image') is-invalid @enderror">
                        <span>{{$article->image}}</span>

                        @error('image')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group col-md-6">
                        <label for="">Creator</label>
                        <input type="text" name="creator" id="inputCreator" class="form-control @error('creator') is-invalid @enderror" value="{{$article->creator}}">

                        @error('image')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group col-md-12">
                        <label for="">Title</label>
                        <input type="text" class="form-control" id="" placeholder="Title" name="title" >
                        <input type="text" name="title" id="inputTitle" class="form-control @error('title') is-invalid @enderror" value="{{$article->title}}">

                        @error('title')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group col-md-12">
                        <label for="exampleTextarea1">Textarea</label>
                        <textarea id="konten" class="form-control @error('content') is-invalid @enderror" name="content" rows="10" cols="50">{!! $article->content !!}</textarea>

                        @error('content')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group col-md-12">
                        <button type="submit" class="btn btn-gradient-primary me-2">Submit</button>
                        <button class="btn btn-light">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
@endsection
