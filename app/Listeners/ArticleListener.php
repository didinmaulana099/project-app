<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Events\ArticleEvent;
use Illuminate\Support\Facades\Cache;

class ArticleListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(ArticleEvent $event)
    {
        //
        $list_articles = Cache::get('article');
        if ($list_articles == null) {
            $list_articles = array();
        }

        array_push($list_articles, $event->article);

        Cache::put('article', $list_articles);
    }
}
